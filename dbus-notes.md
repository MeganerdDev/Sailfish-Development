Notes on dbus

My intention is to send dbus notifications from python scripts.
This way I can cronjob simple scripts to give me updates.

https://talk.maemo.org/showthread.php?t=92303


[nemo@Sailfish ~]$ pkcon install dbus-python3


```
#!/usr/bin/python3
import dbus

bus = dbus.SessionBus()
object = bus.get_object('org.freedesktop.Notifications','/org/freedesktop/Notifications')
interface = dbus.Interface(object,'org.freedesktop.Notifications')
#print(interface.GetCapabilities())

interface.Notify("app_name",
                0,
                "icon-m-notifications",
                "Here is the title",
                "and here the body",
                dbus.Array(["default", ""]),
                dbus.Dictionary({"x-nemo-preview-body": "preview body",
                                 "x-nemo-preview-summary": "preview summary"},
                                 signature='sv'),
                0)
```



**Other dbus things**


**Open URL**

[nemo@Sailfish ~]$ dbus-send --session --type=method_call --dest=org.sailfishos.browser / org.sailfishos.browser.openUrl array:string:"talk.maemo.org"


**Open Mail**

[nemo@Sailfish ~]$ dbus-send --session --type=method_call --dest=com.jolla.email.ui /com/jolla/email/ui com.jolla.email.ui


**Show dbus signals**

[nemo@Sailfish ~]$ dbus-monitor --system



**QML dbus notifications**

```
import org.nemomobile.notifications 1.0

.......
Button {
	Notification {
		id: notification
		category: "x-nemo.example"
		summary: "Notification summary"
		body: "Notification body"
		previewSummary: "Notification preview summary"
		previewBody: "Notification preview body"
		itemCount: 5
		timestamp: "2013-02-20 18:21:00"
		remoteDBusCallServiceName: "org.nemomobile.example"
		remoteDBusCallObjectPath: "/example"
		remoteDBusCallInterface: "org.nemomobile.example"
		remoteDBusCallMethodName: "doSomething"
		remoteDBusCallArguments: [ "argument", 1 ]
		onClicked: console.log("Clicked")
		onClosed: console.log("Closed, reason: " + reason)
	}
	text: "Application notification, ID " + notification.replacesId
	onClicked: notification.publish()
}
```


**Detect phone unlock**

```
[nemo@Sailfish ~]$ dbus-monitor --session | grep "/com/jolla/lipstick"
signal time=1529970065.019872 sender=:1.36 -> destination=(null destination) serial=3831 path=/com/jolla/lipstick; interface=com.jolla.lipstick; member=coverstatus
signal time=1529970065.026802 sender=:1.36 -> destination=(null destination) serial=3832 path=/com/jolla/lipstick; interface=com.jolla.lipstick; member=coverstatus
signal time=1529970065.046189 sender=:1.36 -> destination=(null destination) serial=3833 path=/com/jolla/lipstick; interface=com.jolla.lipstick; member=coverstatus
signal time=1529970065.046254 sender=:1.36 -> destination=(null destination) serial=3834 path=/com/jolla/lipstick; interface=com.jolla.lipstick; member=coverstatus
```