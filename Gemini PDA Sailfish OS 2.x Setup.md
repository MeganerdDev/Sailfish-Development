Notes for Gemini PDA setup with Sailfish OS 2.1.3.7 (Kymijoki) (armv7hl)

**Basic Usage**

**Get root from shell (Set SSH dev passwd in settings first)**
```
[nemo@Sailfish ~]$ devel-su
Password:
[root@Sailfish nemo]#
```

**Refresh packages**
```
[nemo@Sailfish ~]$ pkcon refresh
```

**Update packages**
```
[nemo@Sailfish ~]$ pkcon update
```

**Install RPM packages locally**
```
[nemo@Sailfish ~]$ pkcon install-local filename.rpm
```

**Install RPM packages**
```
[nemo@Sailfish ~]$ pkcon install nano
```

**List installed packages**
```
[nemo@Sailfish ~]$ pkcon get-packages
```

**Rebuild an RPM package**

This is useful for developers. After you deploy a package you can build an RPM easily like this. (Requires rpmrebuild)

```
rpmrebuild --batch thumbterm
```

https://www.systutorials.com/docs/linux/man/1-rpmrebuild/

**Change app menu order**

```
[nemo@Sailfish ~]$ nano /home/nemo/.config/lipstick/applications.menu
```

**Modify LED notifications for Gemini PDA**

I have tested this and it is working, but be warned any typo at all will cause issues.
I made a typo and even when I rebooted the LED would not change settings until I noticed and fixed the typo.
You can of course bring it back up by restoring the original file.

https://together.jolla.com/question/11328/howto-change-notification-led-pattern/

https://jollaconf.blogspot.com/2014/01/beginning-with-jolla-taking-control-of.html

```
[nemo@Sailfish ~]$ cd /etc/mce
[nemo@Sailfish ~]$ cp 20hybris-led.ini /home/nemo/Documents/20hybris-led.ini.bak
[nemo@Sailfish ~]$ nano 20hybris-led.ini

[root@Sailfish nemo]# systemctl restart mce.service
```

**Terminal**

Modified ThumbTerm to restrict screen rotation
* Original source project: https://github.com/OlliV/thumbterm
* terminal.h - static const int defaultFgColor = 15 // White tty text https://www.calmar.ws/vim/256-xterm-24bit-rgb-color-chart.html
* Main.qml - allowedOrientations: Orientation.LandscapeInverted
* Modified RPM: https://gitlab.com/Meganerd.eth/Sailfish-Development/raw/master/thumbterm-2.2.0-1.armv7hl.rpm

**Python Editor**
* tiDEditor - IDE
* PyWarrior - Terminal editor for Python. http://www.mindspring.com/~torajima/pywarrior/pywarrior.html

**Manga Reader**
* FollowMe

**Browser**
* Sailfish Browser
* Web Pirate browser - Works with Youtube better than native SF browser

**Automation**
* Sailcron + Vixiecron - Cronjob manager + GUI for scheduling tasks
* ShellEx - Run scripts from GUI, stdout to textbox on GUI
* Alternative to Cron: timedclient-qt5 - pkcon install timed-qt5-tools

```
[nemo@Sailfish ~]$ timedclient-qt5 -b'TITLE=button0' -e'APPLICATION=nemoalarms;TITLE=Timer;type=countdown;timeOfDay=1;triggerTime=1395217218;ticker=3'
[nemo@Sailfish ~]$ timedclient-qt5 -b'TITLE=button0' -e'APPLICATION=nemoalarms;TITLE=Clock;type=event;timeOfDay=772;ticker=3'
```

**Crypto Portfolio**
* Mr.CryptoRobot - A Pyotherside & QML app for showing crypto updates I made for my needs (not released)

**Encryption**
* Truecrypt
* GnuPG2 - Needs a few dependencies installed before it is usable

**Mount SD Card (Credits to Vader @ OESF)**
```
[nemo@Sailfish ~]$ devel-su
[root@Sailfish nemo]# mkdir -p /media/sdcard
[root@Sailfish nemo]# mount /dev/mmcblk1p1 /media/sdcard

# You may have to add the -t option if your card has an odd format.
```

**Recommended things to do**
* Configure ambience theme with your own wallpaper from the Gallery app
* Setup SSH access - Settings -> Developer Tools -> Set password -> Save
* Storeman - https://openrepos.net client. Install from Jolla app market
* Install the basics: wget, nano, less, htop
* Lock screen rotation to LandscapeInverted only - https://gitlab.com/Meganerd.eth/Sailfish-Development/blob/master/Lock%20screen%20rotation%20on%20SFOS%202.x.md
* Fix camera screen rotation: https://gitlab.com/Meganerd.eth/Sailfish-Development/blob/master/Fix%20camera%20orientation%20for%20SFOS%202.x%20on%20Gemini%20PDA.md
* File Browser - File Manager
* Patch Manager 3 - Scripts for hacking the environment behavior
* Call Recorder - Auto record all calls
* Find My Jolla

**To do list & whats working**

- [x] Fix rotation to stop screen from rotating upside down for main system (See details from repo)

- [ ] Disable or patch caps lock LED; It is constantly lit; mce service does not appear to be controlling this

- [ ] Set keyboard shortcuts

- [x] Kodi v17 - 
    - [ ] Fix Kodi rotation - Display is currently forced upside down
    - [ ] Test https://kodi.wiki/view/Add-on:YouTube

- [x] Youtube - YTPlayer [fork] from Storeman works well (better than the web browsers) but it is not 100%, 720P video lagging (audio is fine)
    - [ ] Fully functional Youtube

- [x] IRC client - IRC for Sailfish; Working nicely, no issues
- [x] 4G - Works out of box
- [x] Receiving calls - Works out of box
- [ ] Making calls - Not working on 4g mode, working when you switch to 3g mode
- [x] SMS - Works out of box
- [x] Bluetooth - Works out of box
- [x] WiFi - Works out of box
- [x] GPS - Works out of box
- [x] USB Tethering - Works out of box
- [ ] WiFi Hotspot - Natve SFOS application doesn't seem to be working. I have not test any other applications to tether. Permissions issue?
- [ ] Native SFOS Terminal is still rotating; Currently thinking I will just compile RPM with the screen orientation for Gemini PDA locked

**Usefull Materials**

Find packages here:
https://openrepos.net

Debian Preview Image Git
https://github.com/gemian/gemini-keyboard-apps/wiki/DebianTP2

Gemini PDA Partition Tool
http://support.planetcom.co.uk/download/partitionTool.html

SFOS Cheat sheet
https://sailfishos.org/wiki/Sailfish_OS_Cheat_Sheet

Gemini PDA Forums
https://www.oesf.org/forum/index.php?showforum=192
https://forum.xda-developers.com/gemini-pda

Python Development
https://wiki.merproject.org/wiki/Sailfish/Python_Development

Guide to installing Debian to Gemini PDA (Do this first)
https://geminiplanet.com/how-to-install-debian-linux-on-the-gemini/

Guide on installing SFOS to Gemini PDA
https://coshacks.blogspot.com/2018/04/gemini-pda-hackers-guide.html
>>>
**Installing Sailfish**
    Install Android + Linux image, set up Debian (at least run resize2fs if you don't run the full script)
    ```sudo resize2fs /dev/mmcblk0p29```

    Make a backup of linux_boot: sudo dd of=linux-boot.img if=/dev/disk/by-partlabel/linux_boot

    Download the sailfish .zip file to Android's Download directory. For some reason TWRP didn't find it if i just copied
    it to Android filesystem via Debian. MicroSD card should also work.

    Boot TWRP.

    Select the .zip from Downloads and install.

    Power off and boot to Linux. Sailfish should start. The tutorial is a bit broken but you should get past it.
    In Sailfish make a backup of linux_boot: dd of=sailfish-boot.img if=/dev/disk/by-partlabel/linux_boot
>>>



**Misc system files**

/etc/profiled/50.sailfish_default.ini - Specify ringtone, alert, etc audio file and volume. You shouldn't need to modify this since the OS does this with a GUI.


