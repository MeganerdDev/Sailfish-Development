This solves the issue of the camera orientation for SFOS 2.1.4.14 on the Gemini PDA.

This was discovered and tested by Vader at OESF forums so credits to him on this!

I am essentially copying and pasting his instructions below.

**This is untested for SFOS 2.1.3.7 but it should work for that release too**


**Original forum thread at OESF**
https://www.oesf.org/forum/index.php?showtopic=35337&st=0&gopid=288503&#entry288503


### Step i)  Download modified CameraPage.qml file from either of the following links

https://www.oesf.org/forum/index.php?act=attach&type=post&id=6197

https://gitlab.com/Meganerd.eth/Sailfish-Development/blob/master/CameraPage.qml



### Step ii) Backup original file, from a terminal do the following commands

**Backup original CameraPage.qml file**
```
[nemo@Sailfish ~]$ devel-su
Password:
[root@Sailfish nemo]#  cp /usr/share/jolla-camera/pages/CameraPage.qml /usr/share/jolla-camera/pages/CameraPage.qml.orig
```


**Overwrite original CamerPage.qml with modified file that you downloaded**
```
[root@Sailfish nemo]# cp -f /home/nemo/Downloads/CameraPage.qml /usr/share/jolla-camera/pages/CameraPage.qml
```


**Then change some settings to enable the full sensor, and make sure focus works. To change settings, in the terminal use:**

**Add/set the values as per this file to change things like video res etc. Remember the " around the ' character otherwise you will get an error.**

```
[root@Sailfish nemo]# dconf write /apps/jolla-camera/primary/image/imageResolution "'2560x1920'"
[root@Sailfish nemo]# dconf write /apps/jolla-camera/primary/image/focusDistanceValues "[8]"
```



**you can see all the modes with:**

```
[root@Sailfish nemo]# droid-camres -w /tmp/a.txt
```


**CameraPage.qml**


```
import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Media 1.0
import com.jolla.camera 1.0
import org.nemomobile.dbus 2.0
import QtMultimedia 5.4
import "capture"
import "gallery"

Page {
    id: page

    property bool windowVisible
    property Item pageStack
    property alias viewfinder: captureView.viewfinder
    property bool galleryActive

    Binding {
        target: window
        property: "galleryActive"
        value: page.galleryActive
    }

    Binding {
        target: window
        property: "galleryVisible"
        value: page.galleryActive || switcherView.moving
    }

    Binding {
        target: window
        property: "galleryIndex"
        value: galleryLoader.item ? galleryLoader.item.currentIndex : 0
    }

    Binding {
        target: window
        property: "captureModel"
        value: galleryLoader.item ? galleryLoader.item.captureModel : null
    }

    allowedOrientations: captureView.inButtonLayout ? page.orientation : Orientation.All

    orientationTransitions: Transition {
        to: 'Portrait,Landscape,PortraitInverted,LandscapeInverted'
        from: 'Portrait,Landscape,PortraitInverted,LandscapeInverted'
        SequentialAnimation {
            PropertyAction {
                target: page
                property: 'orientationTransitionRunning'
                value: true
            }
            FadeAnimation {
                target: page.pageStack
                to: 0
                duration: 150
            }
            PropertyAction {
                target: page
                properties: 'width,height,rotation,orientation'
            }
            FadeAnimation {
                target: page.pageStack
                to: 1
                duration: 150
            }
            PropertyAction {
                target: page
                property: 'orientationTransitionRunning'
                value: false
            }
        }
    }

    Timer {
        running: Qt.application.state != Qt.ApplicationActive && switcherView.currentIndex != 1
        interval: 15*60*1000
        onTriggered: {
            switcherView.currentIndex = 1
        }
    }

    ListView {
        id: switcherView

        width: page.width
        height: page.height

        orientation: ListView.Horizontal
        snapMode: ListView.SnapOneItem
        boundsBehavior: Flickable.StopAtBounds
        highlightRangeMode: ListView.StrictlyEnforceRange
        interactive: (!galleryLoader.item || !galleryLoader.item.positionLocked)
                    && !captureView.recording
        currentIndex: 1
        focus: true

        flickDeceleration: Theme.flickDeceleration
        maximumFlickVelocity: Theme.maximumFlickVelocity 

        // Normally transition is handled through a different path when flicking,
        // avoid slow transition if triggered by ListView for some reason
        highlightMoveDuration: 300

        Keys.onPressed: {
            if (!event.isAutoRepeat && event.key == Qt.Key_Camera) {
                switcherView.currentIndex = 1 // switch to capture mode
            }
        }

        model: VisualItemModel {
            Item {
                id: galleryItem

                width: page.width
                height: page.height

                Loader {
                    id: galleryLoader

                    anchors.fill: parent

                    asynchronous: true
                    visible: switcherView.moving || page.galleryActive
                }

                BusyIndicator {
                    id: galleryIndicator
                    visible: galleryLoader.status == Loader.Loading
                    anchors.centerIn: parent
                    size: BusyIndicatorSize.Large
                    running: true
                }
            }

            CaptureView {
                id: captureView

                readonly property real _viewfinderPosition: orientation == Orientation.Portrait || orientation == Orientation.Landscape
                                                            ? parent.x + x
                                                            : -parent.x - x
                width: page.width
                height: page.height

                active: true

                orientation: Settings.cameraDevice == "secondary"
				? Orientation.LandscapeInverted
				: Orientation.Landscape
                windowVisible: page.windowVisible
                pageRotation: page.rotation
                captureModel: window.captureModel

                visible: switcherView.moving || captureView.active

                onLoaded: {
                    if (galleryLoader.source == "") {
                        galleryLoader.setSource("gallery/GalleryView.qml", { page: page })
                    }
                }

                CameraRollHint { z: 2 }
                CameraModeHint { z: 2 }

                Binding {
                    target: captureView.viewfinder
                    property: "rotation"
                    value: Settings.cameraDevice == "secondary"
				? 0
				: 180
                }

                Binding {
                    target: captureView.viewfinder
                    property: "x"
                    value: captureView.isPortrait
                           ? captureView._viewfinderPosition
                           : 0
                }

                Binding {
                    target: captureView.viewfinder
                    property: "y"
                    value: !captureView.isPortrait
                            ? captureView._viewfinderPosition + (page.orientation == Orientation.Landscape ? captureView.viewfinderOffset : -captureView.viewfinderOffset)
                            : captureView.viewfinderOffset
                }
            }
        }

        onCurrentItemChanged: {
            if (!moving) {
                page.galleryActive = galleryItem.ListView.isCurrentItem
                captureView.active = captureView.ListView.isCurrentItem
            }
        }

        onMovingChanged: {
            if (!moving) {
                page.galleryActive = galleryItem.ListView.isCurrentItem
                captureView.active = captureView.ListView.isCurrentItem
            } else if (captureView.active) {
                if (galleryLoader.source == "") {
                    galleryLoader.setSource("gallery/GalleryView.qml", { page: page })
                } else if (galleryLoader.item) {
                    galleryLoader.item.positionViewAtBeginning()
                }
            }
        }

    }


    DisabledByMdmView {}

    ScreenBlank {
        suspend: (galleryLoader.item && galleryLoader.item.playing)
                    || captureView.camera.videoRecorder.recorderState == CameraRecorder.RecordingState
    }

    DBusAdaptor {
        iface: "com.jolla.camera.ui"
        service: "com.jolla.camera"
        path: "/"

        signal showViewfinder(variant args)
        onShowViewfinder: {
            switcherView.positionViewAtEnd()
            window.activate()
        }

        signal showFrontViewfinder()
        onShowFrontViewfinder: {
            Settings.cameraDevice = "secondary"
            switcherView.positionViewAtEnd()
            window.activate()
        }
    }
}
```


**Credits**
Discovery for fix from Vader at OESF forum on thread https://www.oesf.org/forum/index.php?showtopic=35337