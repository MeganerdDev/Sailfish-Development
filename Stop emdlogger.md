Someone from IRC pointed out the following service was running.

It appears to run on Android, Debian, and SFOS


```
[nemo@Sailfish ~]$ ps aux | grep logger
shell     2039  0.0  0.0  15484  2140 ?        Sl   Jun15   0:00 /vendor/bin/emdlogger1
shell     2042  0.0  0.0  15468  2140 ?        Sl   Jun15   0:00 /vendor/bin/emdlogger3
nemo     14501  0.0  0.0   5324   856 pts/25   S+   18:18   0:00 grep logger
```


Sockets and memory dumps?

```
[nemo@Sailfish ~]$ strings /vendor/bin/emdlogger1
...
socket_local_client
socket_local_server
unknown socket cmds %s
socket command return: %d
socket command return: %d, errno = %d
com.mediatek.mdlogger.socket1
Success to setup socket local server
Fail to setup socket local server, exit(2)
mdlogger socket select start
Failed to write socket, errno=%d
...
...
Memory Dump starts.
missing_folder
Memory dump is saved in: %s
Ram Dump starts.
Ram dump is saved in: %s
/data/mdlog/bootupLog/mdlog1
...
```


What do we say to data collection services?

**kill -9**
```
[root@Sailfish bin]# kill -9 2039
[root@Sailfish bin]# kill -9 2042
```


Now lets make sure we don't have to deal with this again
```
[root@Sailfish data]# cd /vendor/bin
[root@Sailfish bin]# mv emdlogger1 emdlogger1.orig
[root@Sailfish bin]# mv emdlogger2 emdlogger2.orig
[root@Sailfish bin]# mv emdlogger3 emdlogger3.orig
[root@Sailfish bin]# mv emdlogger5 emdlogger5.orig
[root@Sailfish bin]# mv mdlogger mdlogger.orig
```