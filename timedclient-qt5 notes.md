timedclient-qt5, alternative to Cron for SFOS

Cron is running my python application but it is not showing any dbus notifications.

timeclient-qt5 is an alternative that does do this.

Source: https://git.merproject.org/mer-core/timed/blob/master/tools/timedclient/timedclient.cpp

```
[nemo@Sailfish ~]$ pkcon install timed-qt5-tools
```

```
[nemo@Sailfish ~]$ timedclient-qt5 --help
NAME
  timedclient  --  timed test & debugging tool

SYNOPSIS
  timedclient <options>

OPTIONS
  --help   -h                         --  This help text
  --list   -l                         --  List cookies
  --info   -i                         --  List cookies + key attributes
  --show   -L                         --  List cookies + all attributes
  --ping   -p                         --  Show raw alarm queue content

  --add-button=<args>     -b<args>    --  Add button to event
  --add-action=<args>     -a<args>    --  Add action to event
  --add-recurrence=<args> -r<args>    --  Add recurrence to event
  --add-event=<args>      -e<args>    --  Send event to timed
  --get-event=<cookie>    -g<cookie>  --  Show details of event
  --search=<args>         -s<args>    --  List cookies by attributes

  --cancel-event=<cookie> -c<cookie>  --  Cancel one event
  --cancel-events         -C          --  Cancel all events

  --set-snooze=<secs>                 --  Set default snooze value
  --get-snooze                        --  Query default snooze value
  --set-app-snooze=<app:secs>         --  Set app specific snooze value
  --get-app-snooze=<app>              --  Query app specific snooze value

  --set-enabled=<bool>                --  Enable/Disable alarms
  --get-enabled                       --  Query enabled status

  --get-pid                           --  Query PID of timed process

...
...
```


Examples

https://sailfishos.org/wiki/Sailfish_OS_Cheat_Sheet#Show_Dialogs
```
[nemo@Sailfish ~]$ timedclient-qt5 -b'TITLE=button0' -e'APPLICATION=nemoalarms;TITLE=Timer;type=countdown;timeOfDay=1;triggerTime=1395217218;ticker=3'
[nemo@Sailfish ~]$ timedclient-qt5 -b'TITLE=button0' -e'APPLICATION=nemoalarms;TITLE=Clock;type=event;timeOfDay=772;ticker=3'
```

```
[nemo@Sailfish ~]$ timedclient-qt5 --list
[nemo@Sailfish ~]$ timedclient-qt5 --cancel-events
```


**WORKING: Run script with dbus notifications**

```
[nemo@Sailfish ~]$ timedclient-qt5 -a'whenDue;runCommand=sh /home/nemo/Documents/test.sh@nemo' -e'APPLICATION=test;TITLE=Hello;ticker=1'
```

To loop a script for me I simply set the ticker to 3600 and had my script run this command at the end.
Aparently you could also do hour=1;hour=2;hour=3;..etc but I prefer this method since you could have it set the re-occurence time dynamically from your scripts.



events file

```
[nemo@Sailfish Documents]$ more ~/.timed/events.data
events =
[
  
  {
    attr =
    [
       { key = "APPLICATION", val = "test" },
       { key = "TITLE", val = "Hello" }
    ],
    b_attr = [  { attr = [  { key = "TITLE", val = "button0" } ] } ],
    cookie = 28,
    flags = $reminder|$snoozing,
    snooze = [ 1, 0 ],
    ticker = 1529326356
  }
],
next_cookie = 29
.
```

Testing

```
timedclient-qt5 -b'TITLE=button0' -e'APPLICATION=test;TITLE=Hello;ticker=20'
timedclient-qt5 -a'whenDue;runCommand=/usr/share/openrepos-autoambience2/test.sh@nemo' -e'APPLICATION=Autoambience2;TITLE=Autoambience2_test;ticker=120'
timedclient-qt5 -b'TITLE=button0' -a'whenButton=0;runCommand=/home/nemo/Document/test.sh@nemo' -e'APPLICATION=test;TITLE=Hello;ticker=1'
```

Run script

```
[nemo@Sailfish Documents]$ more test.sh
#!/bin/bash
python3 /home/nemo/Documents/test.py


[nemo@Sailfish Documents]$ timedclient-qt5 -b'TITLE=button0' -a'whenDue;runCommand=sh /home/nemo/Documents/test.sh@nemo' -e'APPLICATION=test;TITLE=Hello;ticker=1'


[nemo@Sailfish Documents]$ more ~/.timed/events.data
events =
[
  
  {
    actions =
    [
      
      {
        attr =
        [
           { key = "COMMAND", val = "sh /home/nemo/Documents/test.sh" },
           { key = "USER", val = "nemo" }
        ],
        flags = $run_command|$state_due
      }
    ],
    attr =
    [
       { key = "APPLICATION", val = "test" },
       { key = "TITLE", val = "Hello" }
    ],
    b_attr = [  { attr = [  { key = "TITLE", val = "button0" } ] } ],
    client_creds = { gid = "nemo", uid = "nemo" },
    cookie = 43,
    flags = $reminder|$snoozing,
    snooze = [ 1, 0 ],
    ticker = 1529327766
  }
],
next_cookie = 44
.
```


From an alarm for every monday 9:25 AM

```
[nemo@Sailfish Documents]$ more ~/.timed/events.data
events =
[
  
  {
    attr =
    [
       { key = "APPLICATION", val = "nemoalarms" },
       { key = "TITLE", val = "test" },
       { key = "createdDate", val = "1529328171605" },
       { key = "daysOfWeek", val = "m" },
       { key = "timeOfDay", val = "565" },
       { key = "type", val = "clock" }
    ],
    cookie = 46,
    flags = $alarm|$boot|$keep_alive|$reminder,
    recrs =
    [
      
      {
        hour = $h09,
        mday = $last_day|$whole_month,
        mins = $m25,
        mons = $every_month,
        wday = $mon
      }
    ],
    snooze = [ 1 ],
    tsz_max = 2
  }
],
next_cookie = 47
.
```

Snoozing the alarm

```
[nemo@Sailfish Documents]$ more ~/.timed/events.data
events =
[
  
  {
    attr =
    [
       { key = "APPLICATION", val = "nemoalarms" },
       { key = "TITLE", val = "test" },
       { key = "createdDate", val = "1529328171605" },
       { key = "daysOfWeek", val = "m" },
       { key = "timeOfDay", val = "565" },
       { key = "type", val = "clock" }
    ],
    cookie = 46,
    flags = $alarm|$boot|$keep_alive|$reminder|$snoozing,
    recrs =
    [
      
      {
        hour = $h09,
        mday = $last_day|$whole_month,
        mins = $m25,
        mons = $every_month,
        wday = $mon
      }
    ],
    snooze = [ 1 ],
    ticker = 1529328603,
    tsz_max = 2
  }
],
next_cookie = 47
.
```

Alarm off

```
[nemo@Sailfish Documents]$ more ~/.timed/events.data
events =
[
  
  {
    attr =
    [
       { key = "APPLICATION", val = "nemoalarms" },
       { key = "TITLE", val = "test" },
       { key = "createdDate", val = "1529328171605" },
       { key = "daysOfWeek", val = "m" },
       { key = "timeOfDay", val = "565" },
       { key = "type", val = "clock" }
    ],
    cookie = 47,
    flags = $alarm|$boot|$keep_alive|$reminder,
    snooze = [ 1 ],
    tsz_max = 2
  }
],
next_cookie = 48
.
```

